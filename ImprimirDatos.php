<?php

$Suma = 0;

function ImprimirHeaderTable($Link = 0,$Text = "")
{
    echo "    <table>
    <tr>
    <th>ID</th>
    <th>Producto</th>
    <th>Descripcion</th>
    <th>Precio</th>";
    
    if($Link == 1){
        echo "<th>" . $Text . "</th>";
    }

    echo "</tr>";
}

function ImprimirEndTable()
{
    echo "</table>";
}

function ImprimirProductoTable ($Producto){
    echo "<tr><td>";
    echo $Producto->GetId();
    echo "</td><td>";
    echo $Producto->GetNombre();
    echo "</td><td>";
    echo $Producto->GetDescripcion();
    echo "</td><td>";
    echo $Producto->GetPrecio();
    echo "</td></tr>";      //END
}

function ImprimirCarritoTable ($Producto, $Clave){
    global $Suma;
    
    echo "<tr><td>";
    echo $Producto->GetId();
    echo "</td><td>";
    echo $Producto->GetNombre();
    echo "</td><td>";
    echo $Producto->GetDescripcion();
    echo "</td><td>";
    echo $Producto->GetPrecio();
    echo "</td><td>";
    echo '<a href="DeleteProducto.php?ID=' . $Clave . '">Quitar</a>';
    echo "</td></tr>";      //END
    
    $Suma = $Suma + $Producto->GetPrecio();
}

function ImprimirLineQueryTable ($ID, $Nombre, $Descripcion, $Precio){
    echo "<tr><td>";
    echo $ID;
    echo "</td><td>";
    echo $Nombre;
    echo "</td><td>";
    echo $Descripcion;
    echo "</td><td>";
    echo $Precio;
    echo "</td><td>";
    echo '<a href="AddProducto.php?ID=' . $ID . '">Añadir</a>';
    echo "</td></tr>";      //END
}

function ImprimirArrayProducto($ArrayProducto) {
    ImprimirHeaderTable(0);
    
    foreach ($ArrayProducto as $clave => $valor) {
        ImprimirProductoTable($valor);
    }
    
    ImprimirEndTable();
}

function ImprimirQuery($query) {
    ImprimirHeaderTable(1, "Añadir");
    
    while ($fila = $query->fetch_assoc())
    {
        ImprimirLineQueryTable($fila['codigo'], $fila['producto'], $fila['descripcion'], $fila['precio']);
    }
    
    ImprimirEndTable();
}

function ImprimirCarrito($ArrayProducto) {
    global $Suma;
    
    ImprimirHeaderTable(1, "Quitar");
    
    foreach ($ArrayProducto as $clave => $valor) {
        ImprimirCarritoTable($valor,$clave);
    }
    
    echo "<tr><td>";
    echo "</td><td>";
    echo "</td><td>";
    echo "Total:";
    echo "</td><td>";
    echo $Suma;
    echo "</td><td>";
    echo "</td></tr>";      //END
    
    ImprimirEndTable();
}
?>