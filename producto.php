
<?php
class producto
{
    private $ID;
    private $nombre;
    private $descripcion;
    private $precio;
    
    public function producto($Nombre, $Descripcion, $Precio, $Id = 0)
    {
        $this->nombre      = $Nombre;
        $this->descripcion = $Descripcion;
        $this->precio      = $Precio;
        $this->ID          = $Id;
    }
    
    public function GetNombre()
    {
        return $this->nombre;
    }
    
    public function GetDescripcion()
    {
        return $this->descripcion;
    }
    
    public function GetPrecio()
    {
        return $this->precio;
    }
    
    public function GetId()
    {
        return $this->ID;
    }

}
?>
