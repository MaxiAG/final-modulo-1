<?php
session_start();

class carrito
{
    private static $instancia;
    private $Listado = array();
    
    public static function singleton()
    {
        if (!isset(self::$instancia)) {
            $miclase = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }
    
    private function __construct() {
    }
    
    public function __destruct() {
    }
    
    private function Save() {
        $_SESSION['carrito'] = serialize($this->Listado);
    }
    
    private function Load() {
        if (!isset( $_SESSION['carrito'] )) {
            $_SESSION['carrito'] = serialize($this->Listado);
            echo "Carrito Creado";
        }
        $this->Listado = unserialize( $_SESSION['carrito'] );
    }
    
    public function addProducto ($Producto){
        carrito::Load();
        array_push($this->Listado,$Producto);
        carrito::save();
    }
    
    public function ListProductos() {
        carrito::Load();
        return  $this->Listado;
    }
    
    public function DeleteProducto($Id){
        carrito::Load();
        unset($this->Listado[$Id]);
        carrito::save();
    }
}
?>
