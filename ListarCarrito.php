<?php
require_once 'carrito.php';
?>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>

<h1> Contenido del Carro </h1>
<a href="Inicio.php">Ver Productos</a> </br></br>

<?php
ini_set('display_errors', 'On');
error_reporting(E_ALL | E_STRICT);

include 'producto.php';
include 'ImprimirDatos.php';

$Carro = carrito::singleton();

ImprimirCarrito($Carro->ListProductos());

?>
